var spm = require('../dist/spmodules-bundle.js');
var testing = require('test-bundle')

var chai = testing.chai;

function randomContent() {
    return 'content' + Math.ceil(Math.random() * 100000);
}


function pEql( obj, expect ) {
    for ( var it in expect ) {
        if (expect[it] instanceof Array) {
            for ( var x = 0 ; x < obj.length ; x++ ) {
                pEql( obj[it][x] , expect[it][x] );
            }
        }
        else if (expect[it] != obj[it] ) {
            var e = Error('Expected property ' + it
                + ' = ' + expect[it] + ' got ' + obj[it]);
            e.name = 'pEql';
            throw e;
        }
    }
}

var pathInfo = spm.File.pathInfo;

var out = testing.Tester().test('Basic file saving', function() {
    var content = randomContent();
    var src = spm.File.save('/SuiteScripts/spmTest/file1.txt', content);
    var file = nlapiLoadFile('SuiteScripts/spmTest/file1.txt');
    var val = file.getValue();
    chai.expect(src).is.equal(Number(file.getId()));
    chai.expect(val).is.equal(content);

}).test('pathInfo' , function() {
    spm.File.save('/SuiteScripts/spmTest/t2f1.html', randomContent());
    spm.File.save('/SuiteScripts/spmTest/depth1/t2f2.txt', randomContent());
    spm.File.save('/SuiteScripts/spmTest/depth1/t2f3.jpg', randomContent());
    spm.File.save('/SuiteScripts/spmTest/another/depth1/index.html', randomContent())

    console.log('basic absolute')
    pEql( pathInfo('/SuiteScripts/spmTest/aaaa.txt') , {
        filename : 'aaaa.txt' ,
        fileext : 'txt' ,
        nsfileext : 'PLAINTEXT' ,
        pathabsolute : '/SuiteScripts/spmTest/aaaa.txt' ,
        pathrelative : 'SuiteScripts/spmTest/aaaa.txt' ,
        baseabsolute : '/SuiteScripts/spmTest' ,
        baserelative : 'SuiteScripts/spmTest'
    });

    console.log('basic relative')
    pEql( pathInfo('spmTest/t2f1.html' , '/SuiteScripts' ) , {
        filename : 't2f1.html' ,
        fileext : 'html' ,
        nsfileext : 'HTMLDOC' ,
        pathabsolute : '/SuiteScripts/spmTest/t2f1.html' ,
        pathrelative : 'spmTest/t2f1.html' ,
        baseabsolute : '/SuiteScripts/spmTest' ,
        baserelative : 'spmTest'
    });

    console.log('relative, slash in the end')
    pEql( pathInfo('spmTest/t2f1.html' , '/SuiteScripts/' ) , {
        filename : 't2f1.html' ,
        fileext : 'html' ,
        nsfileext : 'HTMLDOC' ,
        pathabsolute : '/SuiteScripts/spmTest/t2f1.html' ,
        pathrelative : 'spmTest/t2f1.html' ,
        baseabsolute : '/SuiteScripts/spmTest' ,
        baserelative : 'spmTest'
    });

    console.log('no file, no slash')
    chai.expect( pathInfo('/SuiteScripts/spmTest') , {
        filename : 'spmTest' ,
        fileext : null ,
        nsfileext : null ,
        pathabsolute : '/SuiteScripts/spmTest' ,
        pathrelative : 'SuiteScripts/spmTest' ,
        baseabsolute : '/SuiteScripts' ,
        baserelative : 'SuiteScripts'
    });

    console.log('w slash')
    var v = pathInfo('/SuiteScripts/spmTest/');
    //console.log(v)
    pEql( v , {
        filename : null ,
        fileext : null ,
        nsfileext : null ,
        pathabsolute : null ,
        pathrelative : null ,
        baseabsolute : '/SuiteScripts/spmTest' ,
        baserelative : 'SuiteScripts/spmTest'
    });

    console.log('path wildcard, backwards base')
    var v = pathInfo('**/depth1/t2f3.jpg' , '/SuiteScripts/spmTest');
    //console.log(v);
    pEql( v , {
        filename : 't2f3.jpg' ,
        fileext : 'jpg' ,
        nsfileext : 'JPGIMAGE' ,
        pathabsolute : null ,
        pathrelative : null ,
        baserelative : '.' ,
        baseabsolute : '/SuiteScripts/spmTest' ,
        tails : [
            {
                pathabsolute : '/SuiteScripts/spmTest/depth1/t2f3.jpg' ,
                pathrelative : 'depth1/t2f3.jpg' ,
                baserelative : 'depth1' ,
                baseabsolute : '/SuiteScripts/spmTest/depth1'
            } ,
            {
                pathabsolute : '/SuiteScripts/spmTest/another/depth1/t2f3.jpg' ,
                pathrelative : 'another/depth1/t2f3.jpg' ,
                baserelative : 'another/depth1' ,
                baseabsolute : '/SuiteScripts/spmTest/another/depth1'
            }
        ]
    });

    console.log('path wildcard, no folder right-side');
    var v = pathInfo('/SuiteScripts/spmTest/**/t2*');
    pEql( v , {
        filename : 't2*' ,
        fileext : null ,
        nsfileext : null ,
        pathabsolute : null ,
        pathrelative : null ,
        baserelative : 'SuiteScripts/spmTest'
    });

    var m = v.tails.map( function(t) { return t.baseabsolute });
    console.log(m)
    chai.expect(m).to.include('/SuiteScripts/spmTest');
    chai.expect(m).to.include('/SuiteScripts/spmTest/depth1');
    chai.expect(m).to.include('/SuiteScripts/spmTest/another');
    chai.expect(m).to.include('/SuiteScripts/spmTest/another/depth1');

});

module.exports = out;
