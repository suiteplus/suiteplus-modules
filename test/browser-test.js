describe( 'js to xml tests' , function() {

    var spm = spmodules_bundle;
    var convert = spm.JsXml.convert;
    var jsToXml = spm.JsXml.jsToXml;

    it('convert' , () => {

        var i1 = { h1 : 'texto' };
        var o1 = [ {
            tag : 'h1' ,
            children : [{ textnode : 'texto' }]
        }];
        console.log(JSON.stringify(convert(i1)))
        expect(convert(i1)).to.eql(o1);


        var i2 = { h1 : {
            '_class' : 'titulo' ,
            0 : 'texto'
        }};
        var o2 = [{
            tag : 'h1' ,
            attributes : {
                'class' : 'titulo'
            } ,
            children : [
                { textnode : 'texto' }
            ]
        }];
        console.log(JSON.stringify(convert(i2)))
        expect(convert(i2)).to.eql(o2);


        var i3 = { ul : {
            '_class' : 'pai' ,
            0 : 'texto' ,
            li : 'texto li' ,
            a : {
                '_href' : 'google.com' ,
                0 : 'google'
            }
        }};
        var o3 = [{
            tag : 'ul' ,
            attributes : { "class" : 'pai' } ,
            children : [
                { textnode : 'texto' } ,
                {
                    tag : 'li' ,
                    children : [{ textnode : 'texto li' }]
                } ,
                {
                    tag : 'a' ,
                    attributes : {
                        href : 'google.com'
                    } ,
                    children : [{ textnode : "google" }]
                }
            ]
        }]
        console.log(JSON.stringify(convert(i3)))
        expect(convert(i3)).to.eql(o3);

        var i4 = { li : [
            { '_class' : 'list' } ,
            '1',
            '2' ,
            { b : 'negrito' }
        ]};
        var o4 = [{
            tag : 'li' ,
            attributes : { 'class' : 'list' } ,
            children : [
                { textnode : '1' } ,
                { textnode : '2' } ,
                { tag : 'b' , children : [{textnode:'negrito'}] }
            ]
        }];
        console.log(JSON.stringify(convert(i4)))
        expect(convert(i4)).to.eql(o4);

        var i5 = [ { b : '1' } , { b : '2'} , { b : '3' } ];
        var o5 = [
            { tag : 'b' , children : [{textnode : '1'}] } ,
            { tag : 'b' , children : [{textnode : '2'}] } ,
            { tag : 'b' , children : [{textnode : '3'}] }
        ];
        console.log(JSON.stringify(convert(i5)));
        expect(convert(i5)).to.eql(o5);

    })
    
    
    it('jsxml', function() {
        var i3 = { ul : {
            '_class' : 'pai' ,
            0 : 'texto' ,
            li : 'texto li' ,
            a : {
                '_href' : 'google.com' ,
                0 : 'google'
            }
        }};
        var o3 = jsToXml(i3);
        console.log(o3);
    })

});