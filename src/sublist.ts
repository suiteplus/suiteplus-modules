
//implement me
export class Base {
    //abstract
    value(field,line) : string { throw Error('not implemented') }
    setValue(field,line,value) : void { throw Error('not implemented') }
    count() : number { throw Error('not implemented') }
    insert() : void { throw Error('not implemented') }
    getAllFields() : string[] { throw Error('not implemented') }
    idField : string;
}


export class Aux extends Base {


    dump() : any[] {
        var fields = this.getAllFields();
        var out = [];
        var count = this.count();
        for ( var it = 1 ; it <= count ; it++ ) {
            var item = fields.reduce( (before,current) => {
                before[current] = this.value(current,it);
                return before;
            },{});
            out.push(item);
        }
        return out;
    }


    lineFromId(id:number) : number {

        if (!this.idField) throw Error('undefined form.idField');

        var count = this.count();
        if (!id) return null;
        for ( var it = 1 ; it <= count ; it++ ) {
            if ( this.value(this.idField,it) == String(id) ) return it;
        }

    }


    setItem( item : any , id? : number ) : void {

        if (id) item.id = id;
        var line = this.lineFromId(item.id);
        if (!line) {
            this.insert();
            line = this.count();
        }
        for ( var it in item ) {
            this.setValue(it,line,item[it]);
        }

    }


    setItems( items : any[] ) : void {

        items.forEach( i => {
            this.setItem(i)
        })

    }

}