///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
///<reference path="typings/es6-promise/es6-promise.d.ts"/>
///<reference path="typings/jquery/jquery.d.ts"/>
///<reference path="typings/jqueryui/jqueryui.d.ts"/>

import * as Base from './base'

//promise shim
if(typeof window["Promise"] === 'undefined' && typeof window["RSVP"] !== 'undefined') {

    window["Promise"] = window["RSVP"]["Promise"];

}


export function nlapiRequestURLPromise(url,postdata?,headers?) : Promise<nlobjResponse> {

    return new Promise<nlobjResponse>(function(onFulfill,onReject) {

        nlapiRequestURL(url,postdata,headers, (resp : nlobjResponse) => {
            onFulfill(resp);
        });

    });

}


/**
 *  Função testada pra chamar um restlet pelo client
 */
export function callRestlet( scriptIdOrUrl : string , deployment : string|void , postdata : any , op : string = "POST" ) : Promise<any> {

    return new Promise( (ok,fail) => {
        var url;
        if (scriptIdOrUrl.charAt(0) == '/' || scriptIdOrUrl.substr(0,4) == 'http')
            url = scriptIdOrUrl;
        else
            url = Base.getScriptURL("restlet",scriptIdOrUrl, deployment);

        <any>jQuery.ajax({
            url : url ,
            headers : { "User-Agent-x" : "SuiteScript-Call" } ,
            contentType : "application/json" ,
            type : op ,
            data : JSON.stringify(postdata) ,
            success : function(data){
                console.log(data);
                if (data === '') {
                    fail(Base.error('Resposta de restlet vazia. O script está em teste?'));
                }
                if (data.error) fail(data);
                ok(data);
            } ,
            error : function(jqxhr) {

                try {
                    fail(JSON.parse(jqxhr.responseText.replace(/\n/g, "\\n").replace(/\t/g,"\\t")).error);
                } catch(e){
                    fail({ message : jqxhr.responseText })
                }

            }
        });

    });

}

export function permaLoop( condition : (reject:(e:any)=>void) => any , loopTime, timeout ) : Promise<any> {

    var start = new Date().getTime();

    return new Promise( (res,rej) => {
        function tryMe() : any {
            try{
                var now = new Date().getTime();
                if ( now - start > timeout ) return rej('Timeout');
                var status = condition(rej);
                if (!status) return setTimeout(tryMe,loopTime);
                res();
            }catch(e) {
                setTimeout(tryMe,loopTime);
            }
        }
        tryMe();
    });

}


export function onViewInit( fn : (mode:string) => void ) {
    if (typeof nlapiLoadFile !== 'undefined') return;
    var isEdit = /\&e\=T/g.test( window.location.href );
    var isNew = !/id=[0-9]+/g.test( window.location.href )
    if (!isEdit && !isNew ) {
        permaLoop( () => nlapiGetRecordType && nlapiGetRecordType() && window["timeformatwithseconds"] ,
            1000 , 15000 )
        .then( () => fn('edit') )
    }
}


// -- diálogo -- //

var dialogTempl =
    '<div class="grayblock" style="position: fixed;width: 100%;height: 100%;z-index: 999;top: 0px;left: 0px;background-color: rgba(0,0,0,0.5);"></div>' +
    '<div class="diag-wrap" style="position: fixed;width: 400px;border: solid black 1px;padding: 20px;background-color: #EAEAEA;border-radius: 5px;box-shadow: 0px 0px 5px; z-index:1001;">' +
    '<div class="titulo" style="font-weight: bold;position: relative;top: -5px;border-bottom: solid 1px gray;"></div>' +
    '<div class="conteudo" style="margin-top: 10px;font-size: 14px;"></div>' +
    '<div>' +
    '<div class="cancelbtn" style="float: right;margin: 20px 5px 0px 5px;border: solid 1px gray;padding: 5px 10px 5px 10px;background-color: white;border-radius: 5px; cursor: pointer;">Cancelar</div>' +
    '<div class="okbtn" style="float: right;margin: 20px 5px 0px 5px;border: solid 1px gray;padding: 5px 10px 5px 10px;background-color: white;border-radius: 5px; cursor: pointer;">OK</div>' +
    '</div>' +
    '</div>';

export interface DialogOpts {
    okBtnLabel : string;
    cancelBtn : boolean;
    cancelBtnLabel : string;
    prompt : string;
}

var contents, grayblock;
export function dialog(titulo : string , texto : string , opts) : Promise<any> {

    return new Promise(function(resolve, reject){
        opts = opts || {};
        opts.cancelBtn = (opts.cancelBtn === undefined) ? true : opts.cancelBtn;
        opts.cancelBtnLabel = opts.cancelBtnLabel || 'Cancelar';
        opts.okBtnLabel = opts.okBtnLabel || 'OK';

        if (!contents) {
            contents = jQuery(dialogTempl);
            contents.appendTo('body');
            grayblock = jQuery('div.grayblock');
            contents = jQuery('div.diag-wrap');
            contents.css('left' , ((jQuery('body').width() - 400) / 2 ) + 'px' );
        }

        contents.find('.okbtn')
            .off('click')
            .on('click', function(){
                resolve( { prompt : contents.find('textarea.d_prompt').val() || null } );
                contents.hide();
                grayblock.hide();
            });
        contents.find('.cancelbtn')
            .off('click')
            .on('click' , function() {
                reject();
                contents.hide();
                grayblock.hide();
            });
        contents.find('.okbtn').html( opts.okBtnLabel );
        contents.find('.cancelbtn')
            .css('display', opts.cancelBtn ? 'inline-block' : 'none')
            .html( opts.cancelBtnLabel )
        if (titulo) {
            contents.find('.titulo').html(titulo).show();
        } else {
            contents.find('.titulo').html('').hide();
        }
        contents.find('.conteudo').html(texto.replace(/\n/g, '<br/>'));
        if ( opts.prompt ) {
            var ta = jQuery('<textarea class="d_prompt" style="width:355px;height:100px;margin-top:10px;resize:none;"></textarea>')
                .appendTo(contents.find('.conteudo'))
                .focus();
            if (typeof opts.prompt == 'string') ta.val(opts.prompt);
        }
        //var wh = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        contents.css('top','300px');
        contents.show();
        grayblock.show();
    });

}