export interface FldMeta {

    ID? : string;
    idnorm? : string;

    TYPE : NsFieldTypes;
    LABEL : string;
    FIELDGROUP? : string;
    displaytype? : string;
    storevalue? : boolean;

    sourcelist? : string;
    sourcefrom? : string;

}

export enum NsFieldTypes {

    checkbox = <any>"checkbox" ,
    text = <any>"text" ,
    select = <any>"select" ,
    multiselect = <any>"multiselect" ,
    phone = <any>"phone" ,
    email = <any>"email" ,
    textarea = <any>"textarea" ,

    date = <any> "date" ,
    datetime = <any>"datetime" ,
    datetimetz = <any> "datetimetz" ,
    time = <any> "time" ,

    number = <any>"number" ,
    integer = <any>"integer" ,
    currency = <any>"currency"

}

export enum NsBoolean {
    true = <any>"T" ,
    false = <any>"F"
}