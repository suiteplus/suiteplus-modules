///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>

export function xmldocToJson(xmldoc:any, xpath?:string) {

    var ret = {};
    if (!xpath) xpath = '';

    var cnodes = nlapiSelectNodes(xmldoc, xpath + "/*");

    //we've got a text node!
    if (!cnodes || !cnodes.length) return nlapiSelectValue(xmldoc, xpath);
    for (var it in cnodes) {
        var node = cnodes[it];
        var thisxpath = xpath + "/*[" + (Number(it) + 1) + "]";
        var tag = node.nodeName;
        var content = xmldocToJson(xmldoc, thisxpath);
        if (!ret[tag]) ret[tag] = [];

        //properties

        //objetos do tipo node só podem ser acessados através
        //de alguns médodos da API java apache xerces dom;
        //e não são mapeados pra array javascript pelo rhino
        if (node.attributes && node.attributes.getLength()) {
            for (var it2 = 0; it2 < node.attributes.getLength(); it2++) {
                var attritem = node.attributes.item(it2);
                var attrobj = {};
                content[attritem.getName()] = [attritem.getValue()];
            }
        }

        ret[tag].push(content);

    }

    return ret;
}


export function deepFindKey(json, key) {
    for (var keyit in json) {
        var item = json[keyit];
        var pos = keyit.indexOf(":");
        var keyWithoutNs = ( pos != -1 ) ? keyit.substr(pos + 1) : keyit;
        if (keyWithoutNs == key)
            return item;
        var nest = deepFindKey(item, key);
        if (nest.length)
            return nest;
    }
    return [];
}


export function jxmlFlatten(json) {
    for (var it in json) {
        if (json[it].length == 0) json[it] = "";
        else if (json[it].length == 1) json[it] = jxmlFlatten(json[it][0]);
        else json[it] = jxmlFlatten(json[it]);
    }
    return json;
}


export interface Node {
    tag? : string;
    attributes? : any;
    children? : Node[];
    textnode? : string;
}


export function convert(inp:any) : Node[] {

    var out : Node[] = [];

    for ( var it in inp ) {
        let n : Node = {};
        n.tag = it;
        if (typeof inp[it] == 'string') {
            n.children = [{textnode : inp[it]}];
        }
        else if (typeof inp[it] == 'object') {
            let item = inp[it];
            let attr = {};
            let cnodes : Node[] = [];
            for (let it2 in item) {
                let isNumberTag = (it2 === '0' || Number(it2));
                //nodos de atributo
                if (it2.charAt(0) == '_') attr[it2.substr(1)] = item[it2];
                //nodos de texto
                else if (isNumberTag && typeof item[it2] != 'object') {
                    cnodes = cnodes.concat([{ textnode : item[it2] }])
                } else if (!isNumberTag) {
                    cnodes = cnodes.concat(convert({ [it2] : item[it2] }));
                } else {
                    cnodes = cnodes.concat(convert(item[it2]));
                    //fixme slow
                    let tagcnodes = cnodes.filter( cn => cn.tag && cn.tag.charAt(0) == '_')
                    cnodes = cnodes.filter( cn => !cn.tag || cn.tag.charAt(0) != '_' );
                    tagcnodes.forEach( cn => {
                        attr[cn.tag.substr(1)] = (cn.children && cn.children[0].textnode) || '';
                    })
                }
            }
            if (Object.keys(attr).length) n.attributes = attr;
            if (cnodes.length) n.children = cnodes;
        }
        out.push(n);
    }

    if (inp instanceof Array) {
        out = out.reduce( (bef,curr) => {
            bef = bef.concat( curr.children||[] );
            return bef;
        },[]);
    }

    return out;

}

//    var xmlres = (header) ? '<?xml version="1.0" encoding="UTF-8"?>' : '';

function _jsToXml( obj : Node[] ) : string {
    var out = '';
    obj.forEach( (item:Node) => {
        if (!item.tag) {
            out += item.textnode;
        } else {
            let attr_txt = '';
            let attr_src = item.attributes || {};
            for ( var it in attr_src ) {
                attr_txt += ` ${it}="${attr_src[it]}"`;
            }
            let child_txt = item.children ? _jsToXml(item.children) : '';
            out += `<${item.tag}${attr_txt}>${child_txt}</${item.tag}>`;
        }
    })
    return out;
}

export function jsToXml( obj : any, header : boolean = false ) {
    let converted = convert(obj);
    let out = (header) ? '<?xml version="1.0" encoding="UTF-8"?>' : '';
    return out + _jsToXml(converted);
}