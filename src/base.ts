///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
///<reference path="enums.ts"/>


/**
 *  Base são coisas pequenas ou básicas demais pra merecer um aquivo separado
 */

// -- logs and profiling -- //

declare var GLOBALS;

//instance identifier for logs
export var INUMBER = Math.ceil(Math.random() * 1000);
var lastprofile : Date;

export function log(message) {
    var out = message;
    if ( typeof message == "object" ) out = JSON.stringify(message);
    nlapiLogExecution("DEBUG",`${INUMBER} console.log` , out);
}

export function profile(description?:string) {
    if (lastprofile)
        nlapiLogExecution("DEBUG",`${INUMBER} Profiling: ${description}` , Number(new Date()) - Number(lastprofile));
    lastprofile = new Date();
}

export function logerror(txt) {
    var out = txt;
    if ( typeof txt == "object" ) out = JSON.stringify(txt);
    nlapiLogExecution("ERROR",`${INUMBER} console.error` , out);
}

export function debug() {
    var out = "";

    for ( var it = 0; it < arguments.length ; it++ ) {
        try {
            if (typeof arguments[it] == 'string') out += arguments[it] + "; ";
            else if (arguments[it] instanceof Error) out += arguments[it].message + " - " + arguments[it].stack + "; ";
            else out += JSON.stringify(arguments[it]);
        }catch(e){}
    }

    nlapiLogExecution('DEBUG','debug',out);
}

if (typeof console === 'undefined' && typeof GLOBALS !== 'undefined') {
    GLOBALS.console = <any>{};
    GLOBALS.console.log = log;
    GLOBALS.console.debug = debug;
    GLOBALS.console.profile = profile;
    GLOBALS.console.error = logerror;
}


export function error(message:string,sendMail?:boolean,code?:string) : any {

    if (typeof nlapiLoadFile !== 'undefined') return nlapiCreateError('error'||code, message, !sendMail);
    else return Error(message);
}

export function stackTrace(err : nlobjError|Error) : string {
    if (!(<any>err).getStackTrace) {
        return err['stack'] || '';
    }
    var stack = (<any>err).getStackTrace();
    var out = '';
    for ( var it = 0; it < stack.length; it++) {
        out += stack[it] + ' -- ';
    }
    return out;
}

var _memoCount = 0;
var _memoStore = {};

export function memoizeFunction( fn : any ) : any {

    _memoCount++;
    var fnIdentifier = _memoCount;

    return function() {

        var argarray = [];
        for ( var it = 0 ; it < arguments.length ; it++) argarray.push(arguments[it])

        var hash = String(fnIdentifier) + '|' + argarray.reduce( (bef,curr) => bef + '|' + curr , '');
        if (_memoStore[hash]) return _memoStore[hash];
        _memoStore[hash] = fn.apply(null,argarray);
        return _memoStore[hash];

    }

}


function _getScriptURL(arg1,arg2,arg3) {

    var type,script,deployment;

    if (arguments.length == 1) {
        script = arg1;
    } else {
        type = arg1;
        script = arg2;
        deployment = arg3;
    }

    type = type || 'restlet';
    deployment = deployment || 'customdeploy_' + script.substr('customscript_'.length);

    return nlapiResolveURL(type,script,deployment,'internal');

}

export var getScriptURL = memoizeFunction(_getScriptURL);