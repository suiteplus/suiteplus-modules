///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>

import * as sublist from './sublist';

export class Sublist extends sublist.Aux {

    constructor( public name : string , idField : string = 'id' ) { super() }


    value(field,line) {
        return nlapiGetLineItemValue(this.name,field,line);
    }


    setValue(field,line,value) {
        return nlapiSetLineItemValue(this.name,field,line,value);
    }


    count() {
        return nlapiGetLineItemCount(this.name);
    }


    insert() {
        return nlapiInsertLineItem(this.name)
    }

}