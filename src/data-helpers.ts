///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>

// -- date format -- //

var _myoffset = new Date().getTimezoneOffset()/60;

/**
 * @param datestring - netsuite date string generated server-side
 * @param nsdateformat - date, datetime or datetimetz
 * @param targetTzOffset - date::getTimezoneOffset from target client in hours
 * @returns {string}
 */
export function stringToISODate(datestring:string,nsdateformat:string,targetTzOffset:number) : string {

    var dt = nlapiStringToDate(datestring,nsdateformat);
    dt.setHours(dt.getHours() - (_myoffset - targetTzOffset) );
    return dt.toISOString();

}

/**
 * @param isodate - date generated client-side
 * @param nsdateformat - date, datetime or datetimetz
 * @param targetTzOffset - date::getTimezoneOffset from source client in hours
 * @returns {string}
 */
export function dateToNsDatestring(isodate:string,nsdateformat:string,targetTzOffset:number) : string {

    var dt = new Date(isodate);
    dt.setHours(dt.getHours() + (_myoffset - targetTzOffset) );
    return nlapiDateToString(dt,nsdateformat);

}

/**
 * _.where simplificado que usa comparação fraca (==) ao invés de forte (===)
 */
export function where(arr:Array<any>, filters) {

    var out = [];
    for (var it in arr) {
        var count = 0;
        for (var it2 in filters) {
            if (arr[it][it2] == filters[it2]) count++;
        }
        if (count == Object.keys(filters).length) out.push(arr[it]);
    }
    return out;

}


export function arraySum(arr:Array<any>) {

    var out = 0;
    for (var it in arr) {
        out += Number(arr[it]);
    }
    return out;

}


export function onlyNumbers(cpf:string):string {

    return cpf.match(/\d+/g).reduce((b, c) => {
        return b + c;
    }, "");

}