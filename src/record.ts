///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>

import * as sublist from './sublist';

export class Sublist extends sublist.Aux {


    constructor( public nlobjRecord : nlobjRecord , public name : string ,
                 public idField : string = 'id' ) { super() }


    value(field,line) {
        return this.nlobjRecord.getLineItemValue(this.name,field,line)
    }


    setValue(field,line,value) {
        if (value === undefined) return;
        return this.nlobjRecord.setLineItemValue(this.name,field,line,value||'')
    }


    count() {
        return this.nlobjRecord.getLineItemCount(this.name)
    }


    insert() {
        return this.nlobjRecord.insertLineItem(this.name)
    }

    getAllFields() {
        return this.nlobjRecord.getAllLineItemFields(this.name);
    }


}