var gulp = require('gulp')
    , ts = require('gulp-typescript')
    , merge = require('merge2')
    , gscripts = require('gulp-scripts')
    , nscabinet = require('nscabinet')
    , request = require('request')
    , strip = require('gulp-strip-comments')
    , rename = require('gulp-rename')

gulp.task( 'dist-new' , function() {

    var tsres = gulp.src('src/*.ts')
        .pipe( ts({
            module : 'commonjs' ,
            declaration : true ,
            target : 'es5' ,
            noImplicitUseStrict : true
        }));

    return merge([
        tsres.js.pipe( gulp.dest('dist') ) ,
        tsres.dts.pipe( gulp.dest('dist/dts') ) ,
        gulp.src('src/typings/**/*.*').pipe( gulp.dest('dist/dts/typings') )
    ])

});


gulp.task('package' , () => {
    return gulp.src('dist/spmodules-bundle.js')
        .pipe(gscripts.package())
        .pipe(gscripts.addGlobals())
        .pipe(gulp.dest('dist/browser'))
})


gulp.task('test' , () => {

    return gscripts.runTest({
        localPath : 'test/main-test.js' ,
        destPath : '/SuiteScripts/tester' ,
        scriptId : 503
    }).catch( err => console.log(err) )

})