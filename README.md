# Suiteplus modules

Este repositório busca concentrar funções de uso comum em scripts do netsuite (suitescript).

Aqui atualmente estão reunidas algumas funcionalidades que já foram usadas nos projetos ATER, CNA-Card e SENAR,
mas que até o momento eram copiadas e coladas de um projeto para outro, o que gerava certa confusão
no controle das versões.

# Required

Seu projeto deve ser escrito usando o padrão de dependências do node.js
e empacotado usando o browserify.

# Uso

Adicione a dependência ao seu projeto usando o NPM.

	npm install este-repositório
    
Caso tenha problemas de autenticação com o bitbucket você pode baixar
este repositório e adicionar a dependência localmente (workaround).

    npm install ../suiteplus-modules
	
Utilize o `https://bitbucket.org/willian_k/gulp-scripts` ou outra solução semelhante
para o uso do padrão de dependências do node no seu projeto.

    var spm = require('suiteplus-modules');
    var search = spm.Search.bigSearch('my_record');

  
# base

    console.log
    console.error
    console.debug
    console.profile

  
Quando console não é definido no ambiente, as funções acima são redefinidas,
roteando para `nlapiLogExecution`.

    error(message:string,sendMail?:boolean) : any
    
Roteia para `nlapiCreateError` no servidor ou `Error` no client.

    stackTrace(err : nlobjError) : string
    
Extrai uma stack strace de um `nlobjError`. Linhas são quebradas por ` -- `
para permitir uso em logs do netsuite.

# Client

### Promise shim

Caso o navegador usado não possua suporte a promises e a biblioteca RSVP esteja definida,
aplica o shim de Promise baseado nessa biblioteca.

### Funções

```
nlapiRequestURLPromise(url,postdata?,headers?) : Promise<nlobjResponse>
```
```
callRestlet( scriptIdOrURL : string , deployment : string|void , postdata : any , op : string = "POST" ) : Promise<any>
```
callRestlet chama um restlet da forma esperada pelo netsuite (evitando unexpected error)
e realiza manipulação dos dados retornados de modo que os erros sejam tratados de
forma consistente.

Por exemplo, se o script executado retornar erro, este será roteado para o método
`catch` da promise.


    permaLoop( condition : (reject:(e:any)=>void) => any , loopTime, timeout ) : Promise<any>

Executa a função indicada a cada X milisegundos, até que essa retorne verdadeiro ou que reject()
seja chamado.

OBS: se a função subir erro, o loop não é cancelado. Pra se ter esse comportamento, você tem que
explicitamente fechar a função em um try catch e chamar reject.


    onViewInit( fn : (mode:string) => void )
    
No modo de edição, redireciona para o método page Init. Em modo de visualização,
(onde page init não funciona) faz suas checagens e chama a função apenas após as
coisas do netsuite terem terminado de carregar.


# Search

    bigSearch( recordtype : string , filters : any , columns : nlobjSearchColumn[] ) : nlobjSearchResult[]
    //alias: big

Executa busca sem o limite de 1000 resultados.

  
  
    searchCols( colunas : (string|nlobjSearchColumn)[] ) : nlobjSearchColumn[]
    //alias: cols

Usado para encurtar a sintaxe de busca.
  
  
    searchResToCollection ( result : nlobjSearchResult ) : any
    //alias: collection

Converte um nlobjSearchResult em uma coleção { nome-coluna : valor-coluna , ... }[]
     
Se o resultado contiver joins de 1 para um, a coluna em questão é adicionada ao
resultado com o nome 'nome-join.nome-coluna'.
     
O internalid é mapeado para a variável `id`.
     
Os resultados de `getText(...)` são mapeados dentro da variável `textref`.
  

# File

    pathInfo(pathIn, basePath = '/', createFolders = false) : IPathInfo
    
    export interface IPathInfo {
        folderid: number;
        filename: string;
        fileext: string;
        nsfileext: string;
        pathabsolute: string;
        pathrelative: string;
        baserelative: string;
    }

Função usada no `nscabinet`, trás informações de um caminho com relação a um
caminho base, opcionalmente cria as pastas necessárias para que este caminho exista.

O caminho base deve iniciar em '/' se informado.

    saveFile(path : string, contents : string) : number
    
Salva um arquivo baseado em um caminho, criando pastas se necessário.
O caminho deve iniciar com `/`.

# JsXml

Conversão entre JSON e xml. Apenas usável em scripts de **servidor**. 

    xmldocToJson(xmldoc:Node, xpath?:string) : any

Envie um documento obtido a partir de `nlapiStringToXML`.

    deepFindKey(json, key) : any

Similar à chamada `//key` do xpath, exceto que também ignora os namespaces.

    jxmlFlatten(json) : any

Planifica uma estrutura JSON gerada por xmldocToJson, tornando a navegação mais simples,
mas talvez exigindo checagem de Array.

    jsToXml(any)

Converte um objeto em uma representação xml. Tenta-se usar uma representação simples do xml,
com o mínimo uso de arrays/coleções.

Chaves iniciadas com '_' são tratadas como atributos;


```
{ xml : {
    _xmlns : 'https://ns.com'
    a : {
      _href : '#xml' ,
      0 : 'texto do link'
    } ,
    h1 : 'título' ,
    1 : 'texto' ,
    2 : [
    	{ outro : 'elemento' } ,
        'mais um nodo de texto'
    ]
}
```


# RtMeta

Funções de conversão utilizadas no SENAR Mobile.

    processingIn( fldmeta : enums.FldMeta[] , fldmap , targetTzOffset : number ) : (item_in:any) => any
    
A partir de um conjunto de metadados, retorna uma função que recebe um objeto de
dados proveniente do cliente e o converte pra um objeto de dados amigável
para inserção no netsuite.

Um objeto de dados de entrada possui:

 - chaves mapeadas com nomes curtos (ex: sem o prefixo customrecord)
 
 - Valores formatados para uso experado fora do netsuite (números são convertidos
   para number, datas para ISO string e com timezone corrigida)
   
Um objeto de dados de saída possui:

 - Chaves correspondentes aos nomes dos campos no netsuite (incluindo prefixos);
 
 - Valores formatados para inserção no netsuite (datas para data do netsuite,
   booleanos para T e F).
   
   
    processingOut ( fldmeta : enums.FldMeta[] , targetTzOffset : number ) : (item_out:any) => any
    
O caso inverso.

### FldMap e FldMeta

São metadados extraídos a partir do script __sptools Recordtype Metadata ST__,
instalado a partir do bundle __sptools__.

Neste script, deve-se fornecer como entrada um JSON com formato...

```typescript
interface input {
    recordtypes : {
        id : string; //id do record type record type custom
        getMeta : boolean; //se deseja incluir FLDMETA, selecione true
        formXmlLink : string; //se deseja extrair o field group em que cada campo
        //está, forneça o link + &xml=T
        rtXmlLink : string; //link da tela de configuração do recordtype + &xml=T
        //, obrigatório se getMeta = true.
        selectFields? : string[]; //opcional: ao inv;es de incluir todos os campos,
        //especifique quais
        isList? : boolean; //caso deseje que sejam incluídos os dados do RT
        //na saída, selecione true. Esta opção sempre é tida como verdadeira se
        //o nome do RT começar com customlist
    }[]   
}
```

Na resposta desse, a chave FLD corresponde ao parâmetro fldmap acima, e FLDMETA
a um objeto do tipo enums.FldMeta.


# DataHelpers

    where(arr:Array<any>, filters)

_.where simplificado que usa comparação fraca (==) ao invés de forte (===)

```
arraySum(arr:Array<any>)
onlyNumbers(cpf:string):string
```

```
stringToISODate(datestring:string,nsdateformat:string,targetTzOffset:number) : string
dateToNsDatestring(isodate:string,nsdateformat:string,targetTzOffset:number) : string
```
  
Conversão de data no formato de string do netsuite para data ISO.
Quando rodando em scripts de servidor, `targettzoffset` necessário para fazer
o ajuste de timezone. Obtenha este valor no browser a partir de
`new Date().getTimezoneOffset() / 60`.


# Sublist

Busca simplificar a manipulação de sublistas e unificar as interfaces se sublista
de recordtype e sublista de formulário.

Ex:

  - Extrair lista para um objeto javascript;

  - Interfaces comuns entre sublista de formulário e de record type.

```typescript
interface SublistAux {
	dump(fields:string[]) : any[];
	lineFromId(id:number) : number;
	setItem(item:any) : void;
	setItems(items:any[]) : void;
}
```

### A partir de um formulário (client script)

    var sl = new spm.Form.Sublist('recmachcustrecord_campo');
    //constructor( public name : string , idField : string = 'id' )

### A partir de um nlobjRecord

    var sl = new spm.Record.Sublist(rec,'recmachcustrecord_campo');
    constructor( public nlobjRecord : nlobjRecord , public name : string ,
        public idField : string = 'id' )    

## Editando

Para gerar o arquivo compilado (js), execute `gulp dist-new` (após instalar via `npm install`).