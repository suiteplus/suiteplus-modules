var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
//implement me
var Base = (function () {
    function Base() {
    }
    //abstract
    Base.prototype.value = function (field, line) { throw Error('not implemented'); };
    Base.prototype.setValue = function (field, line, value) { throw Error('not implemented'); };
    Base.prototype.count = function () { throw Error('not implemented'); };
    Base.prototype.insert = function () { throw Error('not implemented'); };
    Base.prototype.getAllFields = function () { throw Error('not implemented'); };
    return Base;
}());
exports.Base = Base;
var Aux = (function (_super) {
    __extends(Aux, _super);
    function Aux() {
        _super.apply(this, arguments);
    }
    Aux.prototype.dump = function () {
        var _this = this;
        var fields = this.getAllFields();
        var out = [];
        var count = this.count();
        for (var it = 1; it <= count; it++) {
            var item = fields.reduce(function (before, current) {
                before[current] = _this.value(current, it);
                return before;
            }, {});
            out.push(item);
        }
        return out;
    };
    Aux.prototype.lineFromId = function (id) {
        if (!this.idField)
            throw Error('undefined form.idField');
        var count = this.count();
        if (!id)
            return null;
        for (var it = 1; it <= count; it++) {
            if (this.value(this.idField, it) == String(id))
                return it;
        }
    };
    Aux.prototype.setItem = function (item, id) {
        if (id)
            item.id = id;
        var line = this.lineFromId(item.id);
        if (!line) {
            this.insert();
            line = this.count();
        }
        for (var it in item) {
            this.setValue(it, line, item[it]);
        }
    };
    Aux.prototype.setItems = function (items) {
        var _this = this;
        items.forEach(function (i) {
            _this.setItem(i);
        });
    };
    return Aux;
}(Base));
exports.Aux = Aux;
