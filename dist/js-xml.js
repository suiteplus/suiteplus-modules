///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
function xmldocToJson(xmldoc, xpath) {
    var ret = {};
    if (!xpath)
        xpath = '';
    var cnodes = nlapiSelectNodes(xmldoc, xpath + "/*");
    //we've got a text node!
    if (!cnodes || !cnodes.length)
        return nlapiSelectValue(xmldoc, xpath);
    for (var it in cnodes) {
        var node = cnodes[it];
        var thisxpath = xpath + "/*[" + (Number(it) + 1) + "]";
        var tag = node.nodeName;
        var content = xmldocToJson(xmldoc, thisxpath);
        if (!ret[tag])
            ret[tag] = [];
        //properties
        //objetos do tipo node só podem ser acessados através
        //de alguns médodos da API java apache xerces dom;
        //e não são mapeados pra array javascript pelo rhino
        if (node.attributes && node.attributes.getLength()) {
            for (var it2 = 0; it2 < node.attributes.getLength(); it2++) {
                var attritem = node.attributes.item(it2);
                var attrobj = {};
                content[attritem.getName()] = [attritem.getValue()];
            }
        }
        ret[tag].push(content);
    }
    return ret;
}
exports.xmldocToJson = xmldocToJson;
function deepFindKey(json, key) {
    for (var keyit in json) {
        var item = json[keyit];
        var pos = keyit.indexOf(":");
        var keyWithoutNs = (pos != -1) ? keyit.substr(pos + 1) : keyit;
        if (keyWithoutNs == key)
            return item;
        var nest = deepFindKey(item, key);
        if (nest.length)
            return nest;
    }
    return [];
}
exports.deepFindKey = deepFindKey;
function jxmlFlatten(json) {
    for (var it in json) {
        if (json[it].length == 0)
            json[it] = "";
        else if (json[it].length == 1)
            json[it] = jxmlFlatten(json[it][0]);
        else
            json[it] = jxmlFlatten(json[it]);
    }
    return json;
}
exports.jxmlFlatten = jxmlFlatten;
function convert(inp) {
    var out = [];
    var _loop_1 = function() {
        var n = {};
        n.tag = it;
        if (typeof inp[it] == 'string') {
            n.children = [{ textnode: inp[it] }];
        }
        else if (typeof inp[it] == 'object') {
            var item = inp[it];
            var attr_1 = {};
            var cnodes = [];
            for (var it2 in item) {
                var isNumberTag = (it2 === '0' || Number(it2));
                //nodos de atributo
                if (it2.charAt(0) == '_')
                    attr_1[it2.substr(1)] = item[it2];
                else if (isNumberTag && typeof item[it2] != 'object') {
                    cnodes = cnodes.concat([{ textnode: item[it2] }]);
                }
                else if (!isNumberTag) {
                    cnodes = cnodes.concat(convert((_a = {}, _a[it2] = item[it2], _a)));
                }
                else {
                    cnodes = cnodes.concat(convert(item[it2]));
                    //fixme slow
                    var tagcnodes = cnodes.filter(function (cn) { return cn.tag && cn.tag.charAt(0) == '_'; });
                    cnodes = cnodes.filter(function (cn) { return !cn.tag || cn.tag.charAt(0) != '_'; });
                    tagcnodes.forEach(function (cn) {
                        attr_1[cn.tag.substr(1)] = (cn.children && cn.children[0].textnode) || '';
                    });
                }
            }
            if (Object.keys(attr_1).length)
                n.attributes = attr_1;
            if (cnodes.length)
                n.children = cnodes;
        }
        out.push(n);
    };
    for (var it in inp) {
        _loop_1();
    }
    if (inp instanceof Array) {
        out = out.reduce(function (bef, curr) {
            bef = bef.concat(curr.children || []);
            return bef;
        }, []);
    }
    return out;
    var _a;
}
exports.convert = convert;
//    var xmlres = (header) ? '<?xml version="1.0" encoding="UTF-8"?>' : '';
function _jsToXml(obj) {
    var out = '';
    obj.forEach(function (item) {
        if (!item.tag) {
            out += item.textnode;
        }
        else {
            var attr_txt = '';
            var attr_src = item.attributes || {};
            for (var it in attr_src) {
                attr_txt += " " + it + "=\"" + attr_src[it] + "\"";
            }
            var child_txt = item.children ? _jsToXml(item.children) : '';
            out += "<" + item.tag + attr_txt + ">" + child_txt + "</" + item.tag + ">";
        }
    });
    return out;
}
function jsToXml(obj, header) {
    if (header === void 0) { header = false; }
    var converted = convert(obj);
    var out = (header) ? '<?xml version="1.0" encoding="UTF-8"?>' : '';
    return out + _jsToXml(converted);
}
exports.jsToXml = jsToXml;
