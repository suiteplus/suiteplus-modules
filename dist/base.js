///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
///<reference path="enums.ts"/>
//instance identifier for logs
exports.INUMBER = Math.ceil(Math.random() * 1000);
var lastprofile;
function log(message) {
    var out = message;
    if (typeof message == "object")
        out = JSON.stringify(message);
    nlapiLogExecution("DEBUG", exports.INUMBER + " console.log", out);
}
exports.log = log;
function profile(description) {
    if (lastprofile)
        nlapiLogExecution("DEBUG", exports.INUMBER + " Profiling: " + description, Number(new Date()) - Number(lastprofile));
    lastprofile = new Date();
}
exports.profile = profile;
function logerror(txt) {
    var out = txt;
    if (typeof txt == "object")
        out = JSON.stringify(txt);
    nlapiLogExecution("ERROR", exports.INUMBER + " console.error", out);
}
exports.logerror = logerror;
function debug() {
    var out = "";
    for (var it = 0; it < arguments.length; it++) {
        try {
            if (typeof arguments[it] == 'string')
                out += arguments[it] + "; ";
            else if (arguments[it] instanceof Error)
                out += arguments[it].message + " - " + arguments[it].stack + "; ";
            else
                out += JSON.stringify(arguments[it]);
        }
        catch (e) { }
    }
    nlapiLogExecution('DEBUG', 'debug', out);
}
exports.debug = debug;
if (typeof console === 'undefined' && typeof GLOBALS !== 'undefined') {
    GLOBALS.console = {};
    GLOBALS.console.log = log;
    GLOBALS.console.debug = debug;
    GLOBALS.console.profile = profile;
    GLOBALS.console.error = logerror;
}
function error(message, sendMail, code) {
    if (typeof nlapiLoadFile !== 'undefined')
        return nlapiCreateError('error' || code, message, !sendMail);
    else
        return Error(message);
}
exports.error = error;
function stackTrace(err) {
    if (!err.getStackTrace) {
        return err['stack'] || '';
    }
    var stack = err.getStackTrace();
    var out = '';
    for (var it = 0; it < stack.length; it++) {
        out += stack[it] + ' -- ';
    }
    return out;
}
exports.stackTrace = stackTrace;
var _memoCount = 0;
var _memoStore = {};
function memoizeFunction(fn) {
    _memoCount++;
    var fnIdentifier = _memoCount;
    return function () {
        var argarray = [];
        for (var it = 0; it < arguments.length; it++)
            argarray.push(arguments[it]);
        var hash = String(fnIdentifier) + '|' + argarray.reduce(function (bef, curr) { return bef + '|' + curr; }, '');
        if (_memoStore[hash])
            return _memoStore[hash];
        _memoStore[hash] = fn.apply(null, argarray);
        return _memoStore[hash];
    };
}
exports.memoizeFunction = memoizeFunction;
function _getScriptURL(arg1, arg2, arg3) {
    var type, script, deployment;
    if (arguments.length == 1) {
        script = arg1;
    }
    else {
        type = arg1;
        script = arg2;
        deployment = arg3;
    }
    type = type || 'restlet';
    deployment = deployment || 'customdeploy_' + script.substr('customscript_'.length);
    return nlapiResolveURL(type, script, deployment, 'internal');
}
exports.getScriptURL = memoizeFunction(_getScriptURL);
