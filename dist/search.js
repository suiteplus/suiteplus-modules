///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
/**
 *  Executar busca sem o limite de 1000 resulados
 */
function bigSearch(recordtype, filters, columns) {
    var res = nlapiCreateSearch(recordtype, filters, columns).runSearch();
    var res_chunk, start_idx = 0, res_final = [];
    do {
        res_chunk = res.getResults(start_idx, start_idx + 1000) || [];
        res_final = res_final.concat(res_chunk);
        start_idx += 1000;
    } while (res_chunk.length);
    return res_final;
}
exports.bigSearch = bigSearch;
exports.big = bigSearch;
/**
 *  Encurtar a sintaxe de colunas de busca.
 */
function searchCols(colunas) {
    return colunas.map(function (coluna) {
        if (typeof coluna == "string") {
            var split = coluna.split(".");
            if (split[1])
                return new nlobjSearchColumn(split[1], split[0]);
            return new nlobjSearchColumn(split[0]);
        }
        else if (coluna instanceof nlobjSearchColumn) {
            return coluna;
        }
        else
            throw nlapiCreateError("mapSearchCol", "Entrada inválida");
    });
}
exports.searchCols = searchCols;
exports.cols = searchCols;
/**
 * Converte um nlobjSearchResult em uma coleção { nome-coluna : valor-coluna , ... }[]
 * O segundo formato é mais útil pra funções de manipulação (underscore/lodash)
 * Apenas joins de 1 pra 1 suportados, o resultado é colocado em variável com nome join.coluna
 * O ID é mapeado para a variável `id`.
 * Os resultados de `getText()` são mapeados dentro da variável `textref`.
 */
function searchResToCollection(result) {
    var columns = result.getAllColumns() || [];
    var ret = columns.reduce(function (prev, curr) {
        var name, join;
        if (join = curr.getJoin()) {
            name = join + "." + curr.getName();
        }
        else {
            name = curr.getName();
        }
        prev[name] = result.getValue(curr);
        if (result.getText(curr))
            prev.textref[name] = result.getText(curr);
        return prev;
    }, { textref: {} });
    ret["id"] = result.getId();
    return ret;
}
exports.searchResToCollection = searchResToCollection;
exports.collection = searchResToCollection;
