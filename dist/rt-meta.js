///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
var dh = require('./data-helpers');
var enums = require('./enums');
function processingIn(fldmeta, fldmap, targetTzOffset) {
    var fts = enums.NsFieldTypes;
    var myoffset = new Date().getTimezoneOffset() / 60;
    return function (item_in) {
        var out = {};
        if (item_in["id"])
            out["id"] = Number(item_in["id"]);
        for (var it in fldmeta) {
            if (!item_in[it])
                continue;
            if (fldmeta[it].displaytype == "STATICTEXT")
                continue;
            var field = fldmeta[it];
            var fieldid = fldmap[it];
            switch (field.TYPE) {
                case fts.date:
                    out[fieldid] = dh.dateToNsDatestring(item_in[it], "date", targetTzOffset);
                    break;
                case fts.datetime:
                    out[fieldid] = dh.dateToNsDatestring(item_in[it], "datetime", targetTzOffset);
                    break;
                case fts.datetimetz:
                    out[fieldid] = dh.dateToNsDatestring(item_in[it], "datetimetz", targetTzOffset);
                    break;
                case fts.checkbox:
                    out[fieldid] = item_in[it] ? "T" : "F";
                    break;
                case fts.multiselect:
                    out[fieldid] = item_in[it];
                    break;
                default:
                    out[fieldid] = String(item_in[it]);
            }
        }
        return out;
    };
}
exports.processingIn = processingIn;
function processingOut(fldmeta, targetTzOffset) {
    var fts = enums.NsFieldTypes;
    return function (item_in) {
        if (item_in["id"])
            item_in["id"] = Number(item_in["id"]);
        for (var it in fldmeta) {
            if (!item_in[it])
                continue;
            var field = fldmeta[it];
            var dt;
            try {
                switch (field.TYPE) {
                    case fts.date:
                        item_in[it] = dh.stringToISODate(item_in[it], "date", targetTzOffset);
                        break;
                    case fts.datetime:
                        item_in[it] = dh.stringToISODate(item_in[it], "datetime", targetTzOffset);
                        break;
                    case fts.datetimetz:
                        item_in[it] = dh.stringToISODate(item_in[it], "datetimetz", targetTzOffset);
                        break;
                    case fts.number:
                    case fts.integer:
                    case fts.select:
                    case fts.currency:
                        item_in[it] = Number(item_in[it]);
                        break;
                    case fts.checkbox:
                        item_in[it] = (item_in[it] == "T");
                        break;
                    case fts.multiselect:
                        if (!(item_in[it] instanceof Array))
                            item_in[it] = String(item_in[it] || "")
                                .replace(new RegExp(String.fromCharCode(5), "g"), ",").split(",").map(function (el) {
                                return Number(el);
                            });
                        break;
                }
            }
            catch (e) {
                throw e + " " + JSON.stringify(item_in) + " it" + it;
            }
        }
        return item_in;
    };
}
exports.processingOut = processingOut;
/**
 *  fieldMapping => { nome : 'custrecord_nome' , numero : 'custrecord_numero' }
 *  collection => { custrecord_nome : "Meu nome" , custrecord_numero : 123 }
 *  out => { nome : "Meu nome" , numero : 123 }
 */
function fieldMappingApply(fieldMapping) {
    return function (collection) {
        var out = {};
        for (var it in fieldMapping) {
            out[it] = collection[fieldMapping[it]];
        }
        out["id"] = collection.id;
        return out;
    };
}
exports.fieldMappingApply = fieldMappingApply;
var SCRIPT_ID_PREFIXES = ['custrecord', 'customrecord', 'custentity', 'customlist', 'custbody', 'customscript'];
function normalizeScriptId(scriptId, options) {
    if (options === void 0) { options = {}; }
    var prefixes_ = SCRIPT_ID_PREFIXES.concat(options.morePrefixes || []);
    var allPrefixes = [];
    for (var it in SCRIPT_ID_PREFIXES) {
        (options.morePrefixes || []).forEach(function (pref) {
            allPrefixes.push(prefixes_[it] + '_' + pref + '_');
            allPrefixes.push(prefixes_[it] + pref + '_');
            allPrefixes.push(prefixes_[it] + pref);
            allPrefixes.push(prefixes_[it] + '_' + pref);
        });
        allPrefixes.push(prefixes_[it] + '_');
        allPrefixes.push(prefixes_[it]);
    }
    for (var it in allPrefixes) {
        var prefix = allPrefixes[it];
        if (scriptId.substr(0, (prefix).length) == prefix)
            return scriptId.substr(prefix.length);
    }
    return scriptId;
    //throw Error('could not normalize scriptId ' + scriptId);
}
exports.normalizeScriptId = normalizeScriptId;
