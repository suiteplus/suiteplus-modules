(function (NsFieldTypes) {
    NsFieldTypes[NsFieldTypes["checkbox"] = "checkbox"] = "checkbox";
    NsFieldTypes[NsFieldTypes["text"] = "text"] = "text";
    NsFieldTypes[NsFieldTypes["select"] = "select"] = "select";
    NsFieldTypes[NsFieldTypes["multiselect"] = "multiselect"] = "multiselect";
    NsFieldTypes[NsFieldTypes["phone"] = "phone"] = "phone";
    NsFieldTypes[NsFieldTypes["email"] = "email"] = "email";
    NsFieldTypes[NsFieldTypes["textarea"] = "textarea"] = "textarea";
    NsFieldTypes[NsFieldTypes["date"] = "date"] = "date";
    NsFieldTypes[NsFieldTypes["datetime"] = "datetime"] = "datetime";
    NsFieldTypes[NsFieldTypes["datetimetz"] = "datetimetz"] = "datetimetz";
    NsFieldTypes[NsFieldTypes["time"] = "time"] = "time";
    NsFieldTypes[NsFieldTypes["number"] = "number"] = "number";
    NsFieldTypes[NsFieldTypes["integer"] = "integer"] = "integer";
    NsFieldTypes[NsFieldTypes["currency"] = "currency"] = "currency";
})(exports.NsFieldTypes || (exports.NsFieldTypes = {}));
var NsFieldTypes = exports.NsFieldTypes;
(function (NsBoolean) {
    NsBoolean[NsBoolean["true"] = "T"] = "true";
    NsBoolean[NsBoolean["false"] = "F"] = "false";
})(exports.NsBoolean || (exports.NsBoolean = {}));
var NsBoolean = exports.NsBoolean;
