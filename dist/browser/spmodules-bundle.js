
//mod spmodules-bundle
var GLOBALS = this;
require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
///<reference path="enums.ts"/>
//instance identifier for logs
exports.INUMBER = Math.ceil(Math.random() * 1000);
var lastprofile;
function log(message) {
    var out = message;
    if (typeof message == "object")
        out = JSON.stringify(message);
    nlapiLogExecution("DEBUG", exports.INUMBER + " console.log", out);
}
exports.log = log;
function profile(description) {
    if (lastprofile)
        nlapiLogExecution("DEBUG", exports.INUMBER + " Profiling: " + description, Number(new Date()) - Number(lastprofile));
    lastprofile = new Date();
}
exports.profile = profile;
function logerror(txt) {
    var out = txt;
    if (typeof txt == "object")
        out = JSON.stringify(txt);
    nlapiLogExecution("ERROR", exports.INUMBER + " console.error", out);
}
exports.logerror = logerror;
function debug() {
    var out = "";
    for (var it = 0; it < arguments.length; it++) {
        try {
            if (typeof arguments[it] == 'string')
                out += arguments[it] + "; ";
            else if (arguments[it] instanceof Error)
                out += arguments[it].message + " - " + arguments[it].stack + "; ";
            else
                out += JSON.stringify(arguments[it]);
        }
        catch (e) { }
    }
    nlapiLogExecution('DEBUG', 'debug', out);
}
exports.debug = debug;
if (typeof console === 'undefined' && typeof GLOBALS !== 'undefined') {
    GLOBALS.console = {};
    GLOBALS.console.log = log;
    GLOBALS.console.debug = debug;
    GLOBALS.console.profile = profile;
    GLOBALS.console.error = logerror;
}
function error(message, sendMail, code) {
    if (typeof nlapiLoadFile !== 'undefined')
        return nlapiCreateError('error' || code, message, !sendMail);
    else
        return Error(message);
}
exports.error = error;
function stackTrace(err) {
    if (!err.getStackTrace) {
        return err['stack'] || '';
    }
    var stack = err.getStackTrace();
    var out = '';
    for (var it = 0; it < stack.length; it++) {
        out += stack[it] + ' -- ';
    }
    return out;
}
exports.stackTrace = stackTrace;
var _memoCount = 0;
var _memoStore = {};
function memoizeFunction(fn) {
    _memoCount++;
    var fnIdentifier = _memoCount;
    return function () {
        var argarray = [];
        for (var it = 0; it < arguments.length; it++)
            argarray.push(arguments[it]);
        var hash = String(fnIdentifier) + '|' + argarray.reduce(function (bef, curr) { return bef + '|' + curr; }, '');
        if (_memoStore[hash])
            return _memoStore[hash];
        _memoStore[hash] = fn.apply(null, argarray);
        return _memoStore[hash];
    };
}
exports.memoizeFunction = memoizeFunction;
function _getScriptURL(arg1, arg2, arg3) {
    var type, script, deployment;
    if (arguments.length == 1) {
        script = arg1;
    }
    else {
        type = arg1;
        script = arg2;
        deployment = arg3;
    }
    type = type || 'restlet';
    deployment = deployment || 'customdeploy_' + script.substr('customscript_'.length);
    return nlapiResolveURL(type, script, deployment, 'internal');
}
exports.getScriptURL = memoizeFunction(_getScriptURL);

},{}],2:[function(require,module,exports){
///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
///<reference path="typings/es6-promise/es6-promise.d.ts"/>
///<reference path="typings/jquery/jquery.d.ts"/>
///<reference path="typings/jqueryui/jqueryui.d.ts"/>
var Base = require('./base');
//promise shim
if (typeof window["Promise"] === 'undefined' && typeof window["RSVP"] !== 'undefined') {
    window["Promise"] = window["RSVP"]["Promise"];
}
function nlapiRequestURLPromise(url, postdata, headers) {
    return new Promise(function (onFulfill, onReject) {
        nlapiRequestURL(url, postdata, headers, function (resp) {
            onFulfill(resp);
        });
    });
}
exports.nlapiRequestURLPromise = nlapiRequestURLPromise;
/**
 *  Função testada pra chamar um restlet pelo client
 */
function callRestlet(scriptIdOrUrl, deployment, postdata, op) {
    if (op === void 0) { op = "POST"; }
    return new Promise(function (ok, fail) {
        var url;
        if (scriptIdOrUrl.charAt(0) == '/' || scriptIdOrUrl.substr(0, 4) == 'http')
            url = scriptIdOrUrl;
        else
            url = Base.getScriptURL("restlet", scriptIdOrUrl, deployment);
        jQuery.ajax({
            url: url,
            headers: { "User-Agent-x": "SuiteScript-Call" },
            contentType: "application/json",
            type: op,
            data: JSON.stringify(postdata),
            success: function (data) {
                console.log(data);
                if (data === '') {
                    fail(Base.error('Resposta de restlet vazia. O script está em teste?'));
                }
                if (data.error)
                    fail(data);
                ok(data);
            },
            error: function (jqxhr) {
                try {
                    fail(JSON.parse(jqxhr.responseText.replace(/\n/g, "\\n").replace(/\t/g, "\\t")).error);
                }
                catch (e) {
                    fail({ message: jqxhr.responseText });
                }
            }
        });
    });
}
exports.callRestlet = callRestlet;
function permaLoop(condition, loopTime, timeout) {
    var start = new Date().getTime();
    return new Promise(function (res, rej) {
        function tryMe() {
            try {
                var now = new Date().getTime();
                if (now - start > timeout)
                    return rej('Timeout');
                var status = condition(rej);
                if (!status)
                    return setTimeout(tryMe, loopTime);
                res();
            }
            catch (e) {
                setTimeout(tryMe, loopTime);
            }
        }
        tryMe();
    });
}
exports.permaLoop = permaLoop;
function onViewInit(fn) {
    if (typeof nlapiLoadFile !== 'undefined')
        return;
    var isEdit = /\&e\=T/g.test(window.location.href);
    var isNew = !/id=[0-9]+/g.test(window.location.href);
    if (!isEdit && !isNew) {
        permaLoop(function () { return nlapiGetRecordType && nlapiGetRecordType() && window["timeformatwithseconds"]; }, 1000, 15000)
            .then(function () { return fn('edit'); });
    }
}
exports.onViewInit = onViewInit;
// -- diálogo -- //
var dialogTempl = '<div class="grayblock" style="position: fixed;width: 100%;height: 100%;z-index: 999;top: 0px;left: 0px;background-color: rgba(0,0,0,0.5);"></div>' +
    '<div class="diag-wrap" style="position: fixed;width: 400px;border: solid black 1px;padding: 20px;background-color: #EAEAEA;border-radius: 5px;box-shadow: 0px 0px 5px; z-index:1001;">' +
    '<div class="titulo" style="font-weight: bold;position: relative;top: -5px;border-bottom: solid 1px gray;"></div>' +
    '<div class="conteudo" style="margin-top: 10px;font-size: 14px;"></div>' +
    '<div>' +
    '<div class="cancelbtn" style="float: right;margin: 20px 5px 0px 5px;border: solid 1px gray;padding: 5px 10px 5px 10px;background-color: white;border-radius: 5px; cursor: pointer;">Cancelar</div>' +
    '<div class="okbtn" style="float: right;margin: 20px 5px 0px 5px;border: solid 1px gray;padding: 5px 10px 5px 10px;background-color: white;border-radius: 5px; cursor: pointer;">OK</div>' +
    '</div>' +
    '</div>';
var contents, grayblock;
function dialog(titulo, texto, opts) {
    return new Promise(function (resolve, reject) {
        opts = opts || {};
        opts.cancelBtn = (opts.cancelBtn === undefined) ? true : opts.cancelBtn;
        opts.cancelBtnLabel = opts.cancelBtnLabel || 'Cancelar';
        opts.okBtnLabel = opts.okBtnLabel || 'OK';
        if (!contents) {
            contents = jQuery(dialogTempl);
            contents.appendTo('body');
            grayblock = jQuery('div.grayblock');
            contents = jQuery('div.diag-wrap');
            contents.css('left', ((jQuery('body').width() - 400) / 2) + 'px');
        }
        contents.find('.okbtn')
            .off('click')
            .on('click', function () {
            resolve({ prompt: contents.find('textarea.d_prompt').val() || null });
            contents.hide();
            grayblock.hide();
        });
        contents.find('.cancelbtn')
            .off('click')
            .on('click', function () {
            reject();
            contents.hide();
            grayblock.hide();
        });
        contents.find('.okbtn').html(opts.okBtnLabel);
        contents.find('.cancelbtn')
            .css('display', opts.cancelBtn ? 'inline-block' : 'none')
            .html(opts.cancelBtnLabel);
        if (titulo) {
            contents.find('.titulo').html(titulo).show();
        }
        else {
            contents.find('.titulo').html('').hide();
        }
        contents.find('.conteudo').html(texto.replace(/\n/g, '<br/>'));
        if (opts.prompt) {
            var ta = jQuery('<textarea class="d_prompt" style="width:355px;height:100px;margin-top:10px;resize:none;"></textarea>')
                .appendTo(contents.find('.conteudo'))
                .focus();
            if (typeof opts.prompt == 'string')
                ta.val(opts.prompt);
        }
        //var wh = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        contents.css('top', '300px');
        contents.show();
        grayblock.show();
    });
}
exports.dialog = dialog;

},{"./base":1}],3:[function(require,module,exports){
///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
// -- date format -- //
var _myoffset = new Date().getTimezoneOffset() / 60;
/**
 * @param datestring - netsuite date string generated server-side
 * @param nsdateformat - date, datetime or datetimetz
 * @param targetTzOffset - date::getTimezoneOffset from target client in hours
 * @returns {string}
 */
function stringToISODate(datestring, nsdateformat, targetTzOffset) {
    var dt = nlapiStringToDate(datestring, nsdateformat);
    dt.setHours(dt.getHours() - (_myoffset - targetTzOffset));
    return dt.toISOString();
}
exports.stringToISODate = stringToISODate;
/**
 * @param isodate - date generated client-side
 * @param nsdateformat - date, datetime or datetimetz
 * @param targetTzOffset - date::getTimezoneOffset from source client in hours
 * @returns {string}
 */
function dateToNsDatestring(isodate, nsdateformat, targetTzOffset) {
    var dt = new Date(isodate);
    dt.setHours(dt.getHours() + (_myoffset - targetTzOffset));
    return nlapiDateToString(dt, nsdateformat);
}
exports.dateToNsDatestring = dateToNsDatestring;
/**
 * _.where simplificado que usa comparação fraca (==) ao invés de forte (===)
 */
function where(arr, filters) {
    var out = [];
    for (var it in arr) {
        var count = 0;
        for (var it2 in filters) {
            if (arr[it][it2] == filters[it2])
                count++;
        }
        if (count == Object.keys(filters).length)
            out.push(arr[it]);
    }
    return out;
}
exports.where = where;
function arraySum(arr) {
    var out = 0;
    for (var it in arr) {
        out += Number(arr[it]);
    }
    return out;
}
exports.arraySum = arraySum;
function onlyNumbers(cpf) {
    return cpf.match(/\d+/g).reduce(function (b, c) {
        return b + c;
    }, "");
}
exports.onlyNumbers = onlyNumbers;

},{}],4:[function(require,module,exports){
(function (NsFieldTypes) {
    NsFieldTypes[NsFieldTypes["checkbox"] = "checkbox"] = "checkbox";
    NsFieldTypes[NsFieldTypes["text"] = "text"] = "text";
    NsFieldTypes[NsFieldTypes["select"] = "select"] = "select";
    NsFieldTypes[NsFieldTypes["multiselect"] = "multiselect"] = "multiselect";
    NsFieldTypes[NsFieldTypes["phone"] = "phone"] = "phone";
    NsFieldTypes[NsFieldTypes["email"] = "email"] = "email";
    NsFieldTypes[NsFieldTypes["textarea"] = "textarea"] = "textarea";
    NsFieldTypes[NsFieldTypes["date"] = "date"] = "date";
    NsFieldTypes[NsFieldTypes["datetime"] = "datetime"] = "datetime";
    NsFieldTypes[NsFieldTypes["datetimetz"] = "datetimetz"] = "datetimetz";
    NsFieldTypes[NsFieldTypes["time"] = "time"] = "time";
    NsFieldTypes[NsFieldTypes["number"] = "number"] = "number";
    NsFieldTypes[NsFieldTypes["integer"] = "integer"] = "integer";
    NsFieldTypes[NsFieldTypes["currency"] = "currency"] = "currency";
})(exports.NsFieldTypes || (exports.NsFieldTypes = {}));
var NsFieldTypes = exports.NsFieldTypes;
(function (NsBoolean) {
    NsBoolean[NsBoolean["true"] = "T"] = "true";
    NsBoolean[NsBoolean["false"] = "F"] = "false";
})(exports.NsBoolean || (exports.NsBoolean = {}));
var NsBoolean = exports.NsBoolean;

},{}],5:[function(require,module,exports){
///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
var Search = require('./search');
var NON_BINARY_FILETYPES = [
    'CSV',
    'HTMLDOC',
    'JAVASCRIPT',
    'MESSAGERFC',
    'PLAINTEXT',
    'POSTSCRIPT',
    'RTF',
    'SMS',
    'STYLESHEET',
    'XMLDOC'
];
var EXT_TYPES = {
    dwg: 'AUTOCAD',
    bmp: 'BMPIMAGE',
    csv: 'CSV',
    xls: 'EXCEL',
    swf: 'FLASH',
    gif: 'GIFIMAGE',
    gz: 'GZIP',
    htm: 'HTMLDOC',
    html: 'HTMLDOC',
    ico: 'ICON',
    js: 'JAVASCRIPT',
    jpg: 'JPGIMAGE',
    eml: 'MESSAGERFC',
    mp3: 'MP3',
    mpg: 'MPEGMOVIE',
    mpp: 'MSPROJECT',
    pdf: 'PDF',
    pjpeg: 'PJPGIMAGE',
    txt: 'PLAINTEXT',
    png: 'PNGIMAGE',
    ps: 'POSTSCRIPT',
    ppt: 'POWERPOINT',
    mov: 'QUICKTIME',
    rtf: 'RTF',
    sms: 'SMS',
    css: 'STYLESHEET',
    tiff: 'TIFFIMAGE',
    vsd: 'VISIO',
    doc: 'WORD',
    xml: 'XMLDOC',
    zip: 'ZIP'
};
//all folders with absolute path
function allFolders() {
    var _allFolders = Search.big('folder', null, Search.cols(['name', 'parent']));
    var allFolders = _allFolders.map(Search.collection);
    var foldersIdxParent = allFolders.reduce(function (bef, curr) {
        curr.parent = curr.parent || '_ROOT';
        bef[curr.parent] = bef[curr.parent] || [];
        bef[curr.parent].push(curr);
        return bef;
    }, {});
    foldersIdxParent['_ROOT'].forEach(function (item) {
        function swipe(f) {
            if (foldersIdxParent[f.id]) {
                foldersIdxParent[f.id].forEach(function (inner) {
                    inner.abspath = f.abspath + '/' + inner.name;
                    swipe(inner);
                });
            }
        }
        item.abspath = "/" + item.name;
        swipe(item);
    });
    return allFolders;
}
function _relativePath(src, relativeTo) {
    var o;
    //no backwards walking
    if (src.substr(0, relativeTo.length) == relativeTo) {
        o = src.substr(relativeTo.length);
    }
    else {
        // a / b / c1 / d1
        // a / b / d
        var s_src = src.split('/').filter(function (i) { return i == true; });
        var s_rel = relativeTo.split('/').filter(function (i) { return i == true; });
        var count = 0, walk = '';
        for (var x = 0; x < s_src.length; x++) {
            if (s_rel[x] == s_src[x])
                count++;
            else {
                walk += '/' + s_src[x];
            }
        }
        for (var x = 0; x < count; x++) {
            walk = '../' + walk;
        }
        o = walk;
    }
    return o || '.';
}
function pathInfo(pathIn, baseIn, createFolders) {
    if (baseIn === void 0) { baseIn = '/'; }
    if (createFolders === void 0) { createFolders = false; }
    if (pathIn.charAt(0) == '/') {
        pathIn = pathIn.substr(1);
        baseIn = '/';
    }
    if (baseIn.substr(-1) != '/')
        baseIn += '/';
    var absPath = (baseIn + pathIn)
        .replace(/[\\]/g, '/'); //windows fix
    var _split = absPath.split('/');
    var filename = _split[_split.length - 1];
    _split.length = _split.length - 1;
    var absBase = _split.join('/');
    var absBaseSplit = _split.slice(1);
    var hasWildcard = absBaseSplit.some(function (i) { return i == '**'; });
    var _ext = filename ? filename.split('.')[1] : null;
    var prevFolder = null;
    if (!hasWildcard) {
        absBaseSplit.forEach(function (folderName) {
            var filters = [
                ['name', 'is', folderName],
                'and',
                ['parent', 'anyof', (prevFolder || '@NONE@')]
            ];
            var res_folder = nlapiSearchRecord('folder', null, filters);
            if (!res_folder && !createFolders) {
                throw nlapiCreateError('FOLDER_NOT_FOUND', "Folder " + folderName + " not found!", true);
            }
            else if (!res_folder && createFolders) {
                var newFolderRec = nlapiCreateRecord('folder');
                newFolderRec.setFieldValue('name', folderName);
                newFolderRec.setFieldValue('parent', prevFolder);
                prevFolder = nlapiSubmitRecord(newFolderRec);
            }
            else {
                prevFolder = res_folder[0].getId();
            }
        });
        return {
            folderid: prevFolder,
            filename: filename ? filename : null,
            fileext: _ext,
            nsfileext: _ext ? EXT_TYPES[_ext] : null,
            pathabsolute: filename ? absPath : null,
            pathrelative: filename ? _relativePath(absPath, baseIn) : null,
            baseabsolute: absBase,
            baserelative: _relativePath(absBase, baseIn)
        };
    }
    else {
        var preWildcard_1 = '', postWildcard_1 = '', isAfter_1 = false;
        absBaseSplit.forEach(function (item) {
            if (item == '**')
                isAfter_1 = true;
            else if (isAfter_1)
                postWildcard_1 += '/' + item;
            else {
                preWildcard_1 += '/' + item;
            }
        });
        var found = allFolders().filter(function (folder) {
            var pre = !preWildcard_1.length || (folder.abspath.substr(0, preWildcard_1.length) == preWildcard_1);
            var post = !postWildcard_1.length || (folder.abspath.substr(-postWildcard_1.length) == postWildcard_1);
            return pre && post;
        }).map(function (folder) {
            var pabs = filename ? folder.abspath + '/' + filename : null;
            return {
                folderid: folder.id,
                pathabsolute: pabs,
                pathrelative: filename ? _relativePath(pabs, baseIn) : null,
                baseabsolute: folder.abspath,
                baserelative: _relativePath(folder.abspath, baseIn)
            };
        });
        return {
            filename: filename ? filename : null,
            fileext: _ext,
            nsfileext: _ext ? EXT_TYPES[_ext] : null,
            baseabsolute: preWildcard_1,
            baserelative: _relativePath(preWildcard_1, baseIn),
            tails: found
        };
    }
}
exports.pathInfo = pathInfo;
exports.save = saveFile;
function saveFile(path, contents) {
    var info = pathInfo(path, undefined, true);
    var file = nlapiCreateFile(info.filename, info.nsfileext || 'PLAINTEXT', contents);
    file.setFolder(String(info.folderid));
    return Number(nlapiSubmitFile(file));
}
exports.saveFile = saveFile;

},{"./search":10}],6:[function(require,module,exports){
///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sublist = require('./sublist');
var Sublist = (function (_super) {
    __extends(Sublist, _super);
    function Sublist(name, idField) {
        if (idField === void 0) { idField = 'id'; }
        _super.call(this);
        this.name = name;
    }
    Sublist.prototype.value = function (field, line) {
        return nlapiGetLineItemValue(this.name, field, line);
    };
    Sublist.prototype.setValue = function (field, line, value) {
        return nlapiSetLineItemValue(this.name, field, line, value);
    };
    Sublist.prototype.count = function () {
        return nlapiGetLineItemCount(this.name);
    };
    Sublist.prototype.insert = function () {
        return nlapiInsertLineItem(this.name);
    };
    return Sublist;
}(sublist.Aux));
exports.Sublist = Sublist;

},{"./sublist":11}],7:[function(require,module,exports){
///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
function xmldocToJson(xmldoc, xpath) {
    var ret = {};
    if (!xpath)
        xpath = '';
    var cnodes = nlapiSelectNodes(xmldoc, xpath + "/*");
    //we've got a text node!
    if (!cnodes || !cnodes.length)
        return nlapiSelectValue(xmldoc, xpath);
    for (var it in cnodes) {
        var node = cnodes[it];
        var thisxpath = xpath + "/*[" + (Number(it) + 1) + "]";
        var tag = node.nodeName;
        var content = xmldocToJson(xmldoc, thisxpath);
        if (!ret[tag])
            ret[tag] = [];
        //properties
        //objetos do tipo node só podem ser acessados através
        //de alguns médodos da API java apache xerces dom;
        //e não são mapeados pra array javascript pelo rhino
        if (node.attributes && node.attributes.getLength()) {
            for (var it2 = 0; it2 < node.attributes.getLength(); it2++) {
                var attritem = node.attributes.item(it2);
                var attrobj = {};
                content[attritem.getName()] = [attritem.getValue()];
            }
        }
        ret[tag].push(content);
    }
    return ret;
}
exports.xmldocToJson = xmldocToJson;
function deepFindKey(json, key) {
    for (var keyit in json) {
        var item = json[keyit];
        var pos = keyit.indexOf(":");
        var keyWithoutNs = (pos != -1) ? keyit.substr(pos + 1) : keyit;
        if (keyWithoutNs == key)
            return item;
        var nest = deepFindKey(item, key);
        if (nest.length)
            return nest;
    }
    return [];
}
exports.deepFindKey = deepFindKey;
function jxmlFlatten(json) {
    for (var it in json) {
        if (json[it].length == 0)
            json[it] = "";
        else if (json[it].length == 1)
            json[it] = jxmlFlatten(json[it][0]);
        else
            json[it] = jxmlFlatten(json[it]);
    }
    return json;
}
exports.jxmlFlatten = jxmlFlatten;
function convert(inp) {
    var out = [];
    var _loop_1 = function() {
        var n = {};
        n.tag = it;
        if (typeof inp[it] == 'string') {
            n.children = [{ textnode: inp[it] }];
        }
        else if (typeof inp[it] == 'object') {
            var item = inp[it];
            var attr_1 = {};
            var cnodes = [];
            for (var it2 in item) {
                var isNumberTag = (it2 === '0' || Number(it2));
                //nodos de atributo
                if (it2.charAt(0) == '_')
                    attr_1[it2.substr(1)] = item[it2];
                else if (isNumberTag && typeof item[it2] != 'object') {
                    cnodes = cnodes.concat([{ textnode: item[it2] }]);
                }
                else if (!isNumberTag) {
                    cnodes = cnodes.concat(convert((_a = {}, _a[it2] = item[it2], _a)));
                }
                else {
                    cnodes = cnodes.concat(convert(item[it2]));
                    //fixme slow
                    var tagcnodes = cnodes.filter(function (cn) { return cn.tag && cn.tag.charAt(0) == '_'; });
                    cnodes = cnodes.filter(function (cn) { return !cn.tag || cn.tag.charAt(0) != '_'; });
                    tagcnodes.forEach(function (cn) {
                        attr_1[cn.tag.substr(1)] = (cn.children && cn.children[0].textnode) || '';
                    });
                }
            }
            if (Object.keys(attr_1).length)
                n.attributes = attr_1;
            if (cnodes.length)
                n.children = cnodes;
        }
        out.push(n);
    };
    for (var it in inp) {
        _loop_1();
    }
    if (inp instanceof Array) {
        out = out.reduce(function (bef, curr) {
            bef = bef.concat(curr.children || []);
            return bef;
        }, []);
    }
    return out;
    var _a;
}
exports.convert = convert;
//    var xmlres = (header) ? '<?xml version="1.0" encoding="UTF-8"?>' : '';
function _jsToXml(obj) {
    var out = '';
    obj.forEach(function (item) {
        if (!item.tag) {
            out += item.textnode;
        }
        else {
            var attr_txt = '';
            var attr_src = item.attributes || {};
            for (var it in attr_src) {
                attr_txt += " " + it + "=\"" + attr_src[it] + "\"";
            }
            var child_txt = item.children ? _jsToXml(item.children) : '';
            out += "<" + item.tag + attr_txt + ">" + child_txt + "</" + item.tag + ">";
        }
    });
    return out;
}
function jsToXml(obj, header) {
    if (header === void 0) { header = false; }
    var converted = convert(obj);
    var out = (header) ? '<?xml version="1.0" encoding="UTF-8"?>' : '';
    return out + _jsToXml(converted);
}
exports.jsToXml = jsToXml;

},{}],8:[function(require,module,exports){
///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sublist = require('./sublist');
var Sublist = (function (_super) {
    __extends(Sublist, _super);
    function Sublist(nlobjRecord, name, idField) {
        if (idField === void 0) { idField = 'id'; }
        _super.call(this);
        this.nlobjRecord = nlobjRecord;
        this.name = name;
        this.idField = idField;
    }
    Sublist.prototype.value = function (field, line) {
        return this.nlobjRecord.getLineItemValue(this.name, field, line);
    };
    Sublist.prototype.setValue = function (field, line, value) {
        if (value === undefined)
            return;
        return this.nlobjRecord.setLineItemValue(this.name, field, line, value || '');
    };
    Sublist.prototype.count = function () {
        return this.nlobjRecord.getLineItemCount(this.name);
    };
    Sublist.prototype.insert = function () {
        return this.nlobjRecord.insertLineItem(this.name);
    };
    Sublist.prototype.getAllFields = function () {
        return this.nlobjRecord.getAllLineItemFields(this.name);
    };
    return Sublist;
}(sublist.Aux));
exports.Sublist = Sublist;

},{"./sublist":11}],9:[function(require,module,exports){
///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
var dh = require('./data-helpers');
var enums = require('./enums');
function processingIn(fldmeta, fldmap, targetTzOffset) {
    var fts = enums.NsFieldTypes;
    var myoffset = new Date().getTimezoneOffset() / 60;
    return function (item_in) {
        var out = {};
        if (item_in["id"])
            out["id"] = Number(item_in["id"]);
        for (var it in fldmeta) {
            if (!item_in[it])
                continue;
            if (fldmeta[it].displaytype == "STATICTEXT")
                continue;
            var field = fldmeta[it];
            var fieldid = fldmap[it];
            switch (field.TYPE) {
                case fts.date:
                    out[fieldid] = dh.dateToNsDatestring(item_in[it], "date", targetTzOffset);
                    break;
                case fts.datetime:
                    out[fieldid] = dh.dateToNsDatestring(item_in[it], "datetime", targetTzOffset);
                    break;
                case fts.datetimetz:
                    out[fieldid] = dh.dateToNsDatestring(item_in[it], "datetimetz", targetTzOffset);
                    break;
                case fts.checkbox:
                    out[fieldid] = item_in[it] ? "T" : "F";
                    break;
                case fts.multiselect:
                    out[fieldid] = item_in[it];
                    break;
                default:
                    out[fieldid] = String(item_in[it]);
            }
        }
        return out;
    };
}
exports.processingIn = processingIn;
function processingOut(fldmeta, targetTzOffset) {
    var fts = enums.NsFieldTypes;
    return function (item_in) {
        if (item_in["id"])
            item_in["id"] = Number(item_in["id"]);
        for (var it in fldmeta) {
            if (!item_in[it])
                continue;
            var field = fldmeta[it];
            var dt;
            try {
                switch (field.TYPE) {
                    case fts.date:
                        item_in[it] = dh.stringToISODate(item_in[it], "date", targetTzOffset);
                        break;
                    case fts.datetime:
                        item_in[it] = dh.stringToISODate(item_in[it], "datetime", targetTzOffset);
                        break;
                    case fts.datetimetz:
                        item_in[it] = dh.stringToISODate(item_in[it], "datetimetz", targetTzOffset);
                        break;
                    case fts.number:
                    case fts.integer:
                    case fts.select:
                    case fts.currency:
                        item_in[it] = Number(item_in[it]);
                        break;
                    case fts.checkbox:
                        item_in[it] = (item_in[it] == "T");
                        break;
                    case fts.multiselect:
                        if (!(item_in[it] instanceof Array))
                            item_in[it] = String(item_in[it] || "")
                                .replace(new RegExp(String.fromCharCode(5), "g"), ",").split(",").map(function (el) {
                                return Number(el);
                            });
                        break;
                }
            }
            catch (e) {
                throw e + " " + JSON.stringify(item_in) + " it" + it;
            }
        }
        return item_in;
    };
}
exports.processingOut = processingOut;
/**
 *  fieldMapping => { nome : 'custrecord_nome' , numero : 'custrecord_numero' }
 *  collection => { custrecord_nome : "Meu nome" , custrecord_numero : 123 }
 *  out => { nome : "Meu nome" , numero : 123 }
 */
function fieldMappingApply(fieldMapping) {
    return function (collection) {
        var out = {};
        for (var it in fieldMapping) {
            out[it] = collection[fieldMapping[it]];
        }
        out["id"] = collection.id;
        return out;
    };
}
exports.fieldMappingApply = fieldMappingApply;
var SCRIPT_ID_PREFIXES = ['custrecord', 'customrecord', 'custentity', 'customlist', 'custbody', 'customscript'];
function normalizeScriptId(scriptId, options) {
    if (options === void 0) { options = {}; }
    var prefixes_ = SCRIPT_ID_PREFIXES.concat(options.morePrefixes || []);
    var allPrefixes = [];
    for (var it in SCRIPT_ID_PREFIXES) {
        (options.morePrefixes || []).forEach(function (pref) {
            allPrefixes.push(prefixes_[it] + '_' + pref + '_');
            allPrefixes.push(prefixes_[it] + pref + '_');
            allPrefixes.push(prefixes_[it] + pref);
            allPrefixes.push(prefixes_[it] + '_' + pref);
        });
        allPrefixes.push(prefixes_[it] + '_');
        allPrefixes.push(prefixes_[it]);
    }
    for (var it in allPrefixes) {
        var prefix = allPrefixes[it];
        if (scriptId.substr(0, (prefix).length) == prefix)
            return scriptId.substr(prefix.length);
    }
    return scriptId;
    //throw Error('could not normalize scriptId ' + scriptId);
}
exports.normalizeScriptId = normalizeScriptId;

},{"./data-helpers":3,"./enums":4}],10:[function(require,module,exports){
///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
/**
 *  Executar busca sem o limite de 1000 resulados
 */
function bigSearch(recordtype, filters, columns) {
    var res = nlapiCreateSearch(recordtype, filters, columns).runSearch();
    var res_chunk, start_idx = 0, res_final = [];
    do {
        res_chunk = res.getResults(start_idx, start_idx + 1000) || [];
        res_final = res_final.concat(res_chunk);
        start_idx += 1000;
    } while (res_chunk.length);
    return res_final;
}
exports.bigSearch = bigSearch;
exports.big = bigSearch;
/**
 *  Encurtar a sintaxe de colunas de busca.
 */
function searchCols(colunas) {
    return colunas.map(function (coluna) {
        if (typeof coluna == "string") {
            var split = coluna.split(".");
            if (split[1])
                return new nlobjSearchColumn(split[1], split[0]);
            return new nlobjSearchColumn(split[0]);
        }
        else if (coluna instanceof nlobjSearchColumn) {
            return coluna;
        }
        else
            throw nlapiCreateError("mapSearchCol", "Entrada inválida");
    });
}
exports.searchCols = searchCols;
exports.cols = searchCols;
/**
 * Converte um nlobjSearchResult em uma coleção { nome-coluna : valor-coluna , ... }[]
 * O segundo formato é mais útil pra funções de manipulação (underscore/lodash)
 * Apenas joins de 1 pra 1 suportados, o resultado é colocado em variável com nome join.coluna
 * O ID é mapeado para a variável `id`.
 * Os resultados de `getText()` são mapeados dentro da variável `textref`.
 */
function searchResToCollection(result) {
    var columns = result.getAllColumns() || [];
    var ret = columns.reduce(function (prev, curr) {
        var name, join;
        if (join = curr.getJoin()) {
            name = join + "." + curr.getName();
        }
        else {
            name = curr.getName();
        }
        prev[name] = result.getValue(curr);
        if (result.getText(curr))
            prev.textref[name] = result.getText(curr);
        return prev;
    }, { textref: {} });
    ret["id"] = result.getId();
    return ret;
}
exports.searchResToCollection = searchResToCollection;
exports.collection = searchResToCollection;

},{}],11:[function(require,module,exports){
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
//implement me
var Base = (function () {
    function Base() {
    }
    //abstract
    Base.prototype.value = function (field, line) { throw Error('not implemented'); };
    Base.prototype.setValue = function (field, line, value) { throw Error('not implemented'); };
    Base.prototype.count = function () { throw Error('not implemented'); };
    Base.prototype.insert = function () { throw Error('not implemented'); };
    Base.prototype.getAllFields = function () { throw Error('not implemented'); };
    return Base;
}());
exports.Base = Base;
var Aux = (function (_super) {
    __extends(Aux, _super);
    function Aux() {
        _super.apply(this, arguments);
    }
    Aux.prototype.dump = function () {
        var _this = this;
        var fields = this.getAllFields();
        var out = [];
        var count = this.count();
        for (var it = 1; it <= count; it++) {
            var item = fields.reduce(function (before, current) {
                before[current] = _this.value(current, it);
                return before;
            }, {});
            out.push(item);
        }
        return out;
    };
    Aux.prototype.lineFromId = function (id) {
        if (!this.idField)
            throw Error('undefined form.idField');
        var count = this.count();
        if (!id)
            return null;
        for (var it = 1; it <= count; it++) {
            if (this.value(this.idField, it) == String(id))
                return it;
        }
    };
    Aux.prototype.setItem = function (item, id) {
        if (id)
            item.id = id;
        var line = this.lineFromId(item.id);
        if (!line) {
            this.insert();
            line = this.count();
        }
        for (var it in item) {
            this.setValue(it, line, item[it]);
        }
    };
    Aux.prototype.setItems = function (items) {
        var _this = this;
        items.forEach(function (i) {
            _this.setItem(i);
        });
    };
    return Aux;
}(Base));
exports.Aux = Aux;

},{}],"spmodules-bundle":[function(require,module,exports){
var DataHelpers = require('./data-helpers');
exports.DataHelpers = DataHelpers;
var Enums = require('./enums');
exports.Enums = Enums;
var File = require('./file');
exports.File = File;
var Form = require('./form');
exports.Form = Form;
var Record = require('./record');
exports.Record = Record;
var RtMeta = require('./rt-meta');
exports.RtMeta = RtMeta;
var Search = require('./search');
exports.Search = Search;
var Sublist = require('./sublist');
exports.Sublist = Sublist;
var Base = require('./base');
exports.Base = Base;
var Client = require('./client');
exports.Client = Client;
var JsXml = require('./js-xml');
exports.JsXml = JsXml;
var Utils = Base;
exports.Utils = Utils;
var error = Base.error;
exports.error = error;

},{"./base":1,"./client":2,"./data-helpers":3,"./enums":4,"./file":5,"./form":6,"./js-xml":7,"./record":8,"./rt-meta":9,"./search":10,"./sublist":11}]},{},["spmodules-bundle"]);

GLOBALS['spmodules_bundle'] = require('spmodules-bundle');
