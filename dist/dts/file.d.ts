/// <reference path="typings/suitescript/suitescript-1.0.d.ts" />
export interface IPathInfo {
    folderid?: number;
    filename: string | void;
    fileext: string | void;
    nsfileext: string | void;
    pathabsolute?: string | void;
    pathrelative?: string | void;
    baseabsolute: string;
    baserelative: string;
    tails?: IPathInfoTail[];
}
export interface IPathInfoTail {
    folderid: number;
    pathabsolute: string;
    pathrelative: string;
    baserelative: string;
    baseabsolute: string;
}
export declare function pathInfo(pathIn: string, baseIn?: string, createFolders?: boolean): IPathInfo;
export declare var save: typeof saveFile;
export declare function saveFile(path: string, contents: string): number;
