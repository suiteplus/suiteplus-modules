/// <reference path="typings/suitescript/suitescript-1.0.d.ts" />
import * as sublist from './sublist';
export declare class Sublist extends sublist.Aux {
    name: string;
    constructor(name: string, idField?: string);
    value(field: any, line: any): string;
    setValue(field: any, line: any, value: any): void;
    count(): number;
    insert(): void;
}
