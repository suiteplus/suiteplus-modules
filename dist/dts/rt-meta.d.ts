/// <reference path="typings/suitescript/suitescript-1.0.d.ts" />
import * as enums from './enums';
export declare function processingIn(fldmeta: enums.FldMeta[], fldmap: any, targetTzOffset: number): (item_in: any) => any;
export declare function processingOut(fldmeta: enums.FldMeta[], targetTzOffset: number): (item_in: any) => any;
/**
 *  fieldMapping => { nome : 'custrecord_nome' , numero : 'custrecord_numero' }
 *  collection => { custrecord_nome : "Meu nome" , custrecord_numero : 123 }
 *  out => { nome : "Meu nome" , numero : 123 }
 */
export declare function fieldMappingApply(fieldMapping: any): (collection: any) => {};
export interface NormalizeOpts {
    morePrefixes?: string[];
}
export declare function normalizeScriptId(scriptId: string, options?: NormalizeOpts): string;
