/// <reference path="typings/suitescript/suitescript-1.0.d.ts" />
/**
 *  Executar busca sem o limite de 1000 resulados
 */
export declare function bigSearch(recordtype: string, filters: any, columns: nlobjSearchColumn[]): nlobjSearchResult[];
export declare var big: typeof bigSearch;
/**
 *  Encurtar a sintaxe de colunas de busca.
 */
export declare function searchCols(colunas: (string | nlobjSearchColumn)[]): nlobjSearchColumn[];
export declare var cols: typeof searchCols;
/**
 * Converte um nlobjSearchResult em uma coleção { nome-coluna : valor-coluna , ... }[]
 * O segundo formato é mais útil pra funções de manipulação (underscore/lodash)
 * Apenas joins de 1 pra 1 suportados, o resultado é colocado em variável com nome join.coluna
 * O ID é mapeado para a variável `id`.
 * Os resultados de `getText()` são mapeados dentro da variável `textref`.
 */
export declare function searchResToCollection(result: nlobjSearchResult): {
    id: string;
};
export declare var collection: typeof searchResToCollection;
