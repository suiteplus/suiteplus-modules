/// <reference path="typings/suitescript/suitescript-1.0.d.ts" />
/**
 * @param datestring - netsuite date string generated server-side
 * @param nsdateformat - date, datetime or datetimetz
 * @param targetTzOffset - date::getTimezoneOffset from target client in hours
 * @returns {string}
 */
export declare function stringToISODate(datestring: string, nsdateformat: string, targetTzOffset: number): string;
/**
 * @param isodate - date generated client-side
 * @param nsdateformat - date, datetime or datetimetz
 * @param targetTzOffset - date::getTimezoneOffset from source client in hours
 * @returns {string}
 */
export declare function dateToNsDatestring(isodate: string, nsdateformat: string, targetTzOffset: number): string;
/**
 * _.where simplificado que usa comparação fraca (==) ao invés de forte (===)
 */
export declare function where(arr: Array<any>, filters: any): any[];
export declare function arraySum(arr: Array<any>): number;
export declare function onlyNumbers(cpf: string): string;
