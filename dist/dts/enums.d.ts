export interface FldMeta {
    ID?: string;
    idnorm?: string;
    TYPE: NsFieldTypes;
    LABEL: string;
    FIELDGROUP?: string;
    displaytype?: string;
    storevalue?: boolean;
    sourcelist?: string;
    sourcefrom?: string;
}
export declare enum NsFieldTypes {
    checkbox,
    text,
    select,
    multiselect,
    phone,
    email,
    textarea,
    date,
    datetime,
    datetimetz,
    time,
    number,
    integer,
    currency,
}
export declare enum NsBoolean {
    true,
    false,
}
