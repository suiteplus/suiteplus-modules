/// <reference path="typings/suitescript/suitescript-1.0.d.ts" />
export declare function xmldocToJson(xmldoc: any, xpath?: string): {};
export declare function deepFindKey(json: any, key: any): any;
export declare function jxmlFlatten(json: any): any;
export interface Node {
    tag?: string;
    attributes?: any;
    children?: Node[];
    textnode?: string;
}
export declare function convert(inp: any): Node[];
export declare function jsToXml(obj: any, header?: boolean): string;
