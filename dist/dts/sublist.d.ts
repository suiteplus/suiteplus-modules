export declare class Base {
    value(field: any, line: any): string;
    setValue(field: any, line: any, value: any): void;
    count(): number;
    insert(): void;
    getAllFields(): string[];
    idField: string;
}
export declare class Aux extends Base {
    dump(): any[];
    lineFromId(id: number): number;
    setItem(item: any, id?: number): void;
    setItems(items: any[]): void;
}
