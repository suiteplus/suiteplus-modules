/// <reference path="typings/suitescript/suitescript-1.0.d.ts" />
/// <reference path="typings/es6-promise/es6-promise.d.ts" />
/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/jqueryui/jqueryui.d.ts" />
export declare function nlapiRequestURLPromise(url: any, postdata?: any, headers?: any): Promise<nlobjResponse>;
/**
 *  Função testada pra chamar um restlet pelo client
 */
export declare function callRestlet(scriptIdOrUrl: string, deployment: string | void, postdata: any, op?: string): Promise<any>;
export declare function permaLoop(condition: (reject: (e: any) => void) => any, loopTime: any, timeout: any): Promise<any>;
export declare function onViewInit(fn: (mode: string) => void): void;
export interface DialogOpts {
    okBtnLabel: string;
    cancelBtn: boolean;
    cancelBtnLabel: string;
    prompt: string;
}
export declare function dialog(titulo: string, texto: string, opts: any): Promise<any>;
