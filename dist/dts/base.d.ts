/// <reference path="typings/suitescript/suitescript-1.0.d.ts" />
/// <reference path="enums.d.ts" />
export declare var INUMBER: number;
export declare function log(message: any): void;
export declare function profile(description?: string): void;
export declare function logerror(txt: any): void;
export declare function debug(): void;
export declare function error(message: string, sendMail?: boolean, code?: string): any;
export declare function stackTrace(err: nlobjError | Error): string;
export declare function memoizeFunction(fn: any): any;
export declare var getScriptURL: any;
