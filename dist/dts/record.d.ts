/// <reference path="typings/suitescript/suitescript-1.0.d.ts" />
import * as sublist from './sublist';
export declare class Sublist extends sublist.Aux {
    nlobjRecord: nlobjRecord;
    name: string;
    idField: string;
    constructor(nlobjRecord: nlobjRecord, name: string, idField?: string);
    value(field: any, line: any): string;
    setValue(field: any, line: any, value: any): any;
    count(): number;
    insert(): any;
    getAllFields(): any;
}
