///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sublist = require('./sublist');
var Sublist = (function (_super) {
    __extends(Sublist, _super);
    function Sublist(nlobjRecord, name, idField) {
        if (idField === void 0) { idField = 'id'; }
        _super.call(this);
        this.nlobjRecord = nlobjRecord;
        this.name = name;
        this.idField = idField;
    }
    Sublist.prototype.value = function (field, line) {
        return this.nlobjRecord.getLineItemValue(this.name, field, line);
    };
    Sublist.prototype.setValue = function (field, line, value) {
        if (value === undefined)
            return;
        return this.nlobjRecord.setLineItemValue(this.name, field, line, value || '');
    };
    Sublist.prototype.count = function () {
        return this.nlobjRecord.getLineItemCount(this.name);
    };
    Sublist.prototype.insert = function () {
        return this.nlobjRecord.insertLineItem(this.name);
    };
    Sublist.prototype.getAllFields = function () {
        return this.nlobjRecord.getAllLineItemFields(this.name);
    };
    return Sublist;
}(sublist.Aux));
exports.Sublist = Sublist;
