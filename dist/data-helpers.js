///<reference path="typings/suitescript/suitescript-1.0.d.ts"/>
// -- date format -- //
var _myoffset = new Date().getTimezoneOffset() / 60;
/**
 * @param datestring - netsuite date string generated server-side
 * @param nsdateformat - date, datetime or datetimetz
 * @param targetTzOffset - date::getTimezoneOffset from target client in hours
 * @returns {string}
 */
function stringToISODate(datestring, nsdateformat, targetTzOffset) {
    var dt = nlapiStringToDate(datestring, nsdateformat);
    dt.setHours(dt.getHours() - (_myoffset - targetTzOffset));
    return dt.toISOString();
}
exports.stringToISODate = stringToISODate;
/**
 * @param isodate - date generated client-side
 * @param nsdateformat - date, datetime or datetimetz
 * @param targetTzOffset - date::getTimezoneOffset from source client in hours
 * @returns {string}
 */
function dateToNsDatestring(isodate, nsdateformat, targetTzOffset) {
    var dt = new Date(isodate);
    dt.setHours(dt.getHours() + (_myoffset - targetTzOffset));
    return nlapiDateToString(dt, nsdateformat);
}
exports.dateToNsDatestring = dateToNsDatestring;
/**
 * _.where simplificado que usa comparação fraca (==) ao invés de forte (===)
 */
function where(arr, filters) {
    var out = [];
    for (var it in arr) {
        var count = 0;
        for (var it2 in filters) {
            if (arr[it][it2] == filters[it2])
                count++;
        }
        if (count == Object.keys(filters).length)
            out.push(arr[it]);
    }
    return out;
}
exports.where = where;
function arraySum(arr) {
    var out = 0;
    for (var it in arr) {
        out += Number(arr[it]);
    }
    return out;
}
exports.arraySum = arraySum;
function onlyNumbers(cpf) {
    return cpf.match(/\d+/g).reduce(function (b, c) {
        return b + c;
    }, "");
}
exports.onlyNumbers = onlyNumbers;
